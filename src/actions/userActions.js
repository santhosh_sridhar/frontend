import axios from "axios";
import FormData from "form-data";

export const fetchUser = () => {
  return (dispatch) => {
    axios.get("/api/user").then((res) => {
      dispatch({ type: "LOGIN_USER", payload: res.data });
    });
  };
};
export const login = (user) => {
  return (dispatch) => {
    axios
      .post("/api/login", user)
      .then((res) => {
        if (res.data.error)
          dispatch({ type: "LOGIN_USER", payload: { error: res.data.error } });
        else dispatch({ type: "LOGIN_USER", payload: res.data });
      })
      .catch((err) => {
        console.log("/login failed", err);
        dispatch({ type: "LOGIN_USER", payload: { error: err.data } });
      });
  };
};

export const setError = (error) => {
  return { type: "LOGIN_USER", payload: { error } };
};

export const signUp = (user) => {
  return (dispatch) => {
    axios.post("/api/signup", user).then((res) => {
      if (res.data.error)
        dispatch({ type: "SIGNUP_USER", payload: { error: res.data.error } });
      dispatch({ type: "SIGNUP_USER", payload: res.data });
    });
  };
};

export const logout = () => {
  return (dispatch) => {
    axios.get("/api/logout").then((res) => {
      dispatch({ type: "LOGOUT_USER" });
    });
  };
};
export const saveFile = ({ id, file }) => {
  return (dispatch) => {
    const formdata = new FormData();
    formdata.append("userid", id);
    formdata.append("file", file);

    axios
      .post("api/files", formdata, {
        headers: { enctype: "multipart/form-data" },
      })
      .then((res) => {
        if (res.data.error)
          dispatch({ type: "SIGNUP_USER", payload: { error: res.data.error } });
        dispatch({ type: "UPLOAD_FILE", payload: res.data });
      })
      .catch((err) => {
        console.log("/post file error", err);

        dispatch({ type: "UPLOAD_FILE", payload: { error: err } });
      });
  };
};

export const downloadFile = (fileId) => {
  console.log("fileId", fileId);
  return () => {
    axios
      .get(`/api/file/${fileId}`)
      .then((res) => {
        console.log("file downloaded succussfully");
      })
      .catch((err) => {
        console.error("failed to download file", err);
      });
  };
};
