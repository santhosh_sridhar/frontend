const initialState = { id: "", files: [], error: "" };
export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN_USER":
      state = { ...state, ...action.payload };

      break;
    case "SIGNUP_USER":
      state = { ...state, ...action.payload };

      break;
    case "LOGOUT_USER":
      state = { ...initialState };
      break;
    case "UPLOAD_FILE":
      state = { ...state, ...action.payload };
      console.log("state: ", state);
      break;
    default:
      break;
  }
  return state;
};
