import React, { useState } from "react";
import { Button, Card, Form } from "react-bootstrap";

const FileUpload = (props) => {
  const [file, setFile] = useState({});
  const handleFileChange = (event) => {
    const file = event.target.files[0];
    console.log(file);
    setFile(file);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    props.saveFile({ id: props.user.id, file });
    setFile({});
  };
  return (
    <Card className="mt-2">
      <Card.Header>Upload PDF </Card.Header>
      <Card.Body>
        <Form className="my-4" onSubmit={handleSubmit}>
          <Form.File
            name="file"
            accept="application/pdf"
            onChange={handleFileChange}
          ></Form.File>
          <Button size="sm" className="my-4" type="submit" variant="primary">
            Upload
          </Button>
        </Form>
      </Card.Body>
    </Card>
  );
};

export default FileUpload;
