import React, { useState } from "react";
import { Button, Form, Row, Col, Card } from "react-bootstrap";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { signUp, setError } from "../actions/userActions";
const Signup = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handleChange = (event) => {
    if (props.user.error.length) props.setError("");
    if (event.target.id === "txtEmail") setEmail(event.target.value);
    else if (event.target.id === "txtPassword") setPassword(event.target.value);
    else setConfirmPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const isValid = validatePasswords(password, confirmPassword);
    if (isValid) props.signUp({ email, password });
  };

  const validatePasswords = (password, confirmPassword) => {
    if (password.length < 8) {
      props.setError("Password is too short. Minimum 8 char");
      return false;
    }
    if (password !== confirmPassword) {
      props.setError("Passwords doesn't match");
      setConfirmPassword("");
      return false;
    } else return true;
  };
  if (props.user.id) {
    return <Redirect to="/"></Redirect>;
  }
  return (
    <Row>
      <Col xs={{ offset: 3, span: 6 }}>
        <Card className="my-3">
          <Card.Header>SignUp</Card.Header>
          <Card.Body>
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="txtEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  onChange={handleChange}
                  value={email}
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId="txtPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter password"
                  onChange={handleChange}
                  value={password}
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId="txtConfirmPassword">
                <Form.Label>confirm Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Re-enter password"
                  value={confirmPassword}
                  onChange={handleChange}
                  required
                ></Form.Control>
                <small className="text-danger">{props.user.error}</small>
              </Form.Group>
              <Button variant="primary" type="submit">
                SignUp
              </Button>
              <small className="mx-2">
                Already User? <Card.Link href="/login">Login</Card.Link>
              </small>
            </Form>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => {
  return { user: state.user };
};
const mapDispatchToProps = {
  signUp,
  setError,
};
export default connect(mapStateToProps, mapDispatchToProps)(Signup);
