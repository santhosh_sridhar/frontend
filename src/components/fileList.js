import React from "react";
import { Row } from "react-bootstrap";
import File from "./file";

const FileList = (props) => {
  const fileList = props.files.map((file, index) => {
    return (
      <File
        key={index}
        fileId={file.id}
        name={file.name}
        downloadFile={props.downloadFile}
      ></File>
    );
  });
  return <Row>{fileList}</Row>;
};

export default FileList;
