import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  Row,
  Card,
  Col,
  Form,
  FormControl,
  FormGroup,
  FormLabel,
  Button,
} from "react-bootstrap";
import { login, fetchUser } from "../actions/userActions";
import { Redirect } from "react-router-dom";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { fetchUser } = props;
  useEffect(() => {
    console.log("log inside useEffect()");
    fetchUser();
  }, [fetchUser]);
  const handleChange = (event) => {
    if (props.user.error && props.user.error.length) props.user.error = "";
    event.target.id === "txtEmail"
      ? setEmail(event.target.value)
      : setPassword(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    props.login({ email, password });
    setPassword("");
  };
  if (props.user.id) return <Redirect to="/"></Redirect>;
  else
    return (
      <Row>
        <Col xs={{ offset: 6, span: 6 }}>
          <Card className="my-3">
            <Card.Header>Login</Card.Header>
            <Card.Body>
              <Form onSubmit={handleSubmit}>
                <FormGroup controlId="txtEmail">
                  <FormLabel>Email</FormLabel>
                  <FormControl
                    type="email"
                    value={email}
                    placeholder="Enter email"
                    onChange={handleChange}
                    required
                  ></FormControl>
                </FormGroup>
                <FormGroup controlId="txtPassword">
                  <FormLabel>Password</FormLabel>
                  <FormControl
                    type="password"
                    placeholder="Enter password"
                    value={password}
                    onChange={handleChange}
                    required
                  ></FormControl>
                  <small className="text-danger">{props.user.error}</small>
                </FormGroup>
                <Button variant="primary" type="submit">
                  Login
                </Button>
                <small className="mx-2 text-muted">
                  New User? <Card.Link href="/signup">SignUp</Card.Link>
                </small>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    );
};

const mapStateToProps = (state) => {
  return { user: state.user };
};

const mapDispatchToProps = {
  fetchUser,
  login,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
