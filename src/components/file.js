import React from "react";
import { Card } from "react-bootstrap";

const File = (props) => {
  return (
    <Card border="info" style={{ width: "18rem" }} className="mx-2 my-2 col-3">
      <Card.Body>
        <Card.Title>
          <Card.Link
            href={`/api/file/${props.fileId}`}
            download={`${props.name}.pdf`}
          >
            {props.name}
          </Card.Link>
        </Card.Title>
      </Card.Body>
    </Card>
  );
};

export default File;
