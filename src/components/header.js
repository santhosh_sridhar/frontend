import React from "react";
import { Nav, Navbar } from "react-bootstrap";
import { connect } from "react-redux";
import { logout } from "../actions/userActions";

const Header = (props) => {
  const handleClick = () => {
    props.logout();
  };
  return (
    <Navbar variant="dark" bg="dark">
      <Navbar.Brand href="/">PDF Manager</Navbar.Brand>
      {props.user.id && (
        <Nav className="ml-auto">
          <Nav.Link onClick={handleClick}>Logout</Nav.Link>
        </Nav>
      )}
    </Navbar>
  );
};
const mapStateToProps = (state) => {
  return { user: state.user };
};

const mapDispatchToProps = {
  logout,
};
export default connect(mapStateToProps, mapDispatchToProps)(Header);
