import React, { useEffect } from "react";
import FileUpload from "./fileUpload";
import FileList from "./fileList";
import { connect } from "react-redux";
import { Col, Row } from "react-bootstrap";
import { fetchUser, saveFile, downloadFile } from "../actions/userActions";
import { Redirect } from "react-router-dom";

const Home = (props) => {
  const { fetchUser } = props;
  useEffect(() => {
    fetchUser();
  }, [fetchUser]);

  if (!props.user.id) return <Redirect to="/login"></Redirect>;
  return (
    <Row>
      <Col xs={4}>
        <Row>
          <FileUpload user={props.user} saveFile={props.saveFile}></FileUpload>
        </Row>
      </Col>
      <Col xs={8}>
        <FileList files={props.user.files}></FileList>
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => {
  return { user: state.user };
};
const mapDispatchToProps = {
  fetchUser,
  saveFile,
  downloadFile,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
